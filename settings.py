manufacturer_names = ["GE", "Siemens", "ABB"]
pole_choices = [2, 3, 4, 6, 8, 9, 16]
rpm_options = [1800, 3600, 7200, 10800]


# How many times in a given year might an attacker try to get in (Triangle distribution)?
intrusion_attempt_freq = [1, 12, 52]
# And succeed (%)?
intrusion_success_prob = [0, 0.5, 1]

# What are the odds a given device is vulnerable?
vulnerability_prob = [0, 0.5, 1]

# If they get in, what are the odds they'll attack a given device and succeed?
attack_attemp_prob = [0.1, 0.2, 0.3]
attack_success_prob = [0.1, 0.2, 0.3]

# How many devices?
generator_count = 10
breaker_count = 10
stepup_tranformer_count = 10
tx_tranformer_count = 10
stepdown_tranformer_count = 10

# How many times to try
iterations = 100

# At what threshold shall we assume it will happen?
tolerance = 0.01


try:
    from settingslocal import *
except ImportError:
    pass
