import numpy as np
import pandas as pd

from datetime import datetime
from faker import Faker
from pkg_resources import parse_version

from settings import *


def gen_random_firmware():
    return f"{np.random.randint(1,10)}.{np.random.randint(1,10)}.{np.random.randint(1,5)}"


def gen_triangular(params):
    return np.random.triangular(params[0], params[1], params[2])


def gen_generator(fake):
    generator = {"Manufacturer": np.random.choice(manufacturer_names),
                 "Firmware": gen_random_firmware(),
                 "InstallationDate": fake.date_of_birth(maximum_age=50),
                 "Poles": np.random.choice(pole_choices),
                 "MaxRPM": np.random.choice(rpm_options),
                 "MaxAmps": np.random.randint(60, 1000)}
    return generator


def gen_breaker(fake):
    breaker = {"Manufacturer": np.random.choice(manufacturer_names),
               "Firmware": gen_random_firmware(),
               "InstallationDate": fake.date_of_birth(maximum_age=50),
               "MaxVolts": np.random.choice([120, 240,
                                             4000, 13000,
                                             26000, 69000,
                                             138000, 230000,
                                             765000, 500000, 345000]),
               "MaxKVA": np.random.randint(20, 1000)}
    return breaker


def gen_transformer(fake, primary_volts, secondary_volts, kva):
    transformer = {"Manufacturer": np.random.choice(manufacturer_names),
                   "Firmware": gen_random_firmware(),
                   "InstallationDate": fake.date_of_birth(maximum_age=50),
                   "MaxPrimaryVolts": np.random.choice(primary_volts),
                   "MaxSecondaryVolts": np.random.choice(secondary_volts),
                   "MaxKVA": np.random.choice(kva)}
    return transformer


def main():
    fake = Faker()
    pd.set_option('display.max_columns', None)

    generators = []
    for i in range(0, generator_count):
        generators.append(gen_generator(fake))
    generators = pd.DataFrame(generators)

    breakers = []
    for i in range(0, breaker_count):
        breakers.append(gen_breaker(fake))
    breakers = pd.DataFrame(breakers)

    stepup_tranformers = []
    primary_volts = [13800, 22000, 26000]
    secondary_volts = [230000, 345000, 765000]
    kva = [75000, 300000, 500000]
    for i in range(0, stepup_tranformer_count):
        stepup_tranformers.append(gen_transformer(fake, primary_volts, secondary_volts, kva))
    stepup_tranformers = pd.DataFrame(stepup_tranformers)

    transmission_transformers = []
    primary_volts = [230000, 345000, 765000]
    secondary = [115000, 138000, 345000]
    kva = [300000, 500000, 750000]
    for i in range(0, tx_tranformer_count):
        transmission_transformers.append(gen_transformer(fake, primary_volts, secondary_volts, kva))
    transmission_transformers = pd.DataFrame(transmission_transformers)

    stepdown_tranformers = []
    primary_volts = [115000, 138000, 345000]
    secondary = [120, 240, 480]
    kva = [7200, 14400, 28800]
    for i in range(0, stepdown_tranformer_count):
        stepdown_tranformers.append(gen_transformer(fake, primary_volts, secondary_volts, kva))
    stepdown_tranformers = pd.DataFrame(stepdown_tranformers)

    # Scenario 1: GE Generators with a firmware < 5.0 are compromised and the max RPM is dropped to 1337
    iteration_stats = []
    for i in range(0, iterations):
        intrusions = round(gen_triangular(intrusion_attempt_freq) * gen_triangular(intrusion_success_prob))
        generator_status = [0] * generators.shape[0]
        for j in range(0, intrusions):
            for g, generator in generators.iterrows():
                if parse_version(generator.Firmware) < parse_version("5.0"):
                    vulnerability = gen_triangular(vulnerability_prob)
                    attack = gen_triangular(attack_attemp_prob)
                    success = gen_triangular(attack_success_prob)
                    modified = vulnerability * attack * success
                    if modified > tolerance:
                        generator_status[g] += 1

        iteration_stats.append(generator_status)
    iteration_stats = np.array(iteration_stats)
    iteration_stats = np.mean(iteration_stats, axis=0)

    mod_generators = generators.copy()
    for g, generator in generators.iterrows():
        if iteration_stats[g] > 1:
            mod_generators.loc[g, "MaxRPM"] = 1337

    print("Generators Before:")
    print(generators)

    print("Generators After:")
    print(mod_generators)


    # Scenario 2: Breakers made by ABB are compromised and the max voltage is set to 1337
    iteration_stats = []
    for i in range(0, iterations):
        intrusions = round(gen_triangular(intrusion_attempt_freq) * gen_triangular(intrusion_success_prob))
        breaker_status = [0] * breakers.shape[0]
        for j in range(0, intrusions):
            for b, breaker in breakers.iterrows():
                if breaker.Manufacturer == "ABB":
                    vulnerability = gen_triangular(vulnerability_prob)
                    attack = gen_triangular(attack_attemp_prob)
                    success = gen_triangular(attack_success_prob)
                    modified = vulnerability * attack * success
                    if modified > tolerance:
                        breaker_status[b] += 1

        iteration_stats.append(breaker_status)
    iteration_stats = np.array(iteration_stats)
    iteration_stats = np.mean(iteration_stats, axis=0)

    mod_breakers = breakers.copy()
    for b, breaker in breakers.iterrows():
        if iteration_stats[b] > 1:
            mod_breakers.loc[b, "MaxVolts"] = 1337

    print("Breakers Before:")
    print(breakers)

    print("Breakers After:")
    print(mod_breakers)

    # Scenario 3: Siemens Transformers older than 20 years are physically disabled (i.e., max KVA -> 0)
    iteration_stats = []
    for i in range(0, iterations):
        intrusions = round(gen_triangular(intrusion_attempt_freq) * gen_triangular(intrusion_success_prob))
        transformer_status = [0] * breakers.shape[0]
        for j in range(0, intrusions):
            for t, transformer in stepup_tranformers.iterrows():
                age = datetime.now().date() - transformer.InstallationDate
                if age.days / 365.2425 > 20:
                    vulnerability = gen_triangular(vulnerability_prob)
                    attack = gen_triangular(attack_attemp_prob)
                    success = gen_triangular(attack_success_prob)
                    modified = vulnerability * attack * success
                    if modified > tolerance:
                        transformer_status[t] += 1

        iteration_stats.append(transformer_status)
    iteration_stats = np.array(iteration_stats)
    iteration_stats = np.mean(iteration_stats, axis=0)

    mod_stepup_tranformers = stepup_tranformers.copy()
    for t, transformer in stepup_tranformers.iterrows():
        if iteration_stats[t] > 1:
            mod_stepup_tranformers.loc[t, "MaxKVA"] = 0

    print("Step-up Transformers Before:")
    print(stepup_tranformers)

    print("Step-up Transformers After:")
    print(mod_stepup_tranformers)


if __name__ == "__main__":
    main()
